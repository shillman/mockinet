package uk.co.miki.mockservlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MockServlet
 */

public class MockServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;   
	
	private Map<String, String> contextParams = new HashMap<String, String>();
	private String mockFileDirectory = null;
	ServletContext servletContext = null;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MockServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		servletContext = config.getServletContext();
		mockFileDirectory = servletContext.getInitParameter("mockFileDirectory");
		//System.out.println("MOCK FILE DIR: '" + mockFileDirectory + "'");
		
		Enumeration<String> initParams = servletContext.getInitParameterNames();
		
		while (initParams.hasMoreElements()) {
			String path = initParams.nextElement();
			if(! path.equalsIgnoreCase("mockFileDirectory")) {
				String file = servletContext.getInitParameter(path);
				contextParams.put(path, file);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String queryString = request.getQueryString();
		
		if(queryString != null) {
		
			if(queryString.startsWith("mock=")) {
				queryString = queryString.substring(5);
				//System.out.println(queryString);
			
				if(contextParams.containsKey(queryString)) {
				
					String fileName = contextParams.get(queryString);
					//System.out.println("-> " + fileName);
				
					String mockContent = readFile(mockFileDirectory + fileName);
				
					response.setContentType("application/xml");
					response.setCharacterEncoding("UTF-8");
					response.getOutputStream().print(mockContent);
				
				}
			}
		} else {
			
			buildTable(response);
		}
	}

	private void buildTable(HttpServletResponse response) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html><body>");
		sb.append("<table>");
		sb.append("<tr colspan=\"2\">");
		sb.append("<td>");
		sb.append("Usage: /MockServlet?mock=your-mock-name");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr colspan=\"2\">");
		sb.append("<td>");
		sb.append("Serving the following mock files:");
		sb.append("</td>");
		sb.append("</tr>");
		
		Iterator<String> it = contextParams.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			String value = contextParams.get(key);
			sb.append("<tr><td>Name: " + key + "</td><td>File: " + value + "</td></tr>") ;
		}
		
		sb.append("</table>");
		sb.append("</body></html>");
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		response.getOutputStream().print(sb.toString());
	}
	
	private String readFile(String path) {
	
		InputStream inputStream = servletContext.getResourceAsStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        
        String line;
        
        try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
        
        //System.out.println(stringBuilder.toString());   //Prints the string content read from input stream
        
        try {
			reader.close();
		} catch (IOException e) {
			System.out.println(e.toString());
		}
        
        return stringBuilder.toString();
	}

}
